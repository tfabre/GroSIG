package fr.info.grosig.js

import android.content.Context
import android.webkit.JavascriptInterface
import android.location.LocationManager
import android.util.Log

class Location(private val context: Context)
{
    @JavascriptInterface
    fun getLocation(): String
    {
        val location = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        try
        {
            val loc = location.getLastKnownLocation(location.getProviders(true).first())
            return "{ latitude: ${loc?.latitude}, longitude: ${loc?.longitude} }"
        }
        catch (e: SecurityException)
        {
            Log.d("LOCATION_ERROR", e.message)
        }
        return "{}"
    }
}
