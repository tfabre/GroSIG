package fr.info.grosig

import android.os.AsyncTask
import android.util.Log
import java.io.BufferedInputStream
import java.io.FileOutputStream
import java.net.URL

class Downloader(private val file: FileOutputStream): AsyncTask<String, Void, Unit>()
{
    override fun doInBackground(vararg info: String)
    {
        try
        {
            val url = URL(info[0])
            val connection = url.openConnection()
            connection.connect()

            BufferedInputStream(url.openStream(), 1024).use { inputStream ->
                file.use { inputStream.copyTo(it) }
            }
        }
        catch (e: Exception)
        {
            Log.e("DOWNLOAD", "${e.localizedMessage} ${e.message}")
        }
    }
}
