package fr.info.grosig

import android.Manifest
import android.annotation.TargetApi
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AlertDialog
import android.widget.Toast

class PermissionManager(private val context: AppCompatActivity)
{
    @TargetApi(Build.VERSION_CODES.M)
    private fun addPermission(permissionsList: MutableList<String>, permission: String): Boolean
    {
        if (context.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
        {
            permissionsList.add(permission)
            if (!context.shouldShowRequestPermissionRationale(permission))
            {
                return false
            }
        }
        return true
    }

    private fun showMessageOkCancel(message: String, okListener: ((DialogInterface, Int) -> Unit))
    {
        AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("Ok", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show()
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun askForPermission()
    {
        val permissionsNeeded = mutableListOf<String>()
        val permissionsList = mutableListOf<String>()

        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("Show Location")

        if (permissionsList.size > 0)
        {
            if (permissionsNeeded.size > 0)
            {
                var message = "App need access to " + permissionsNeeded[0]

                for (permission in permissionsNeeded)
                    message += message + ", " + permission

                showMessageOkCancel(message, { dialog, which ->
                    context.requestPermissions(permissionsList.toTypedArray(), 124)
                })
                return
            }
            context.requestPermissions(permissionsList.toTypedArray(), 124)
            return
        }
        Toast.makeText(context, "No new Permission Required!", Toast.LENGTH_SHORT)
                .show()
    }
}
