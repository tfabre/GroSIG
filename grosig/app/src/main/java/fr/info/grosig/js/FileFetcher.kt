package fr.info.grosig.js

import android.content.Context
import android.webkit.JavascriptInterface
import fr.info.grosig.MainActivity

class FileFetcher(private val context: Context)
{
    companion object
    {
        private const val LIEU_JUSTICE = "/geo/lieu_justice/bbox/"
        private const val TGI = "/geo/tgi/bbox/"
        private const val TI = "/geo/ti/bbox/"
        private const val GREFFE = "/geo/greffes/bbox/"
        private const val PERSONNE = "/geo/personne/bbox/"
        private const val STATUT = "/statuts"
        private val URL_AND_FILE = hashMapOf(
                LIEU_JUSTICE to MainActivity.LIEU_JUSTICE_FILENAME,
                TGI to MainActivity.TGI_FILENAME,
                TI to MainActivity.TI_FILENAME,
                GREFFE to MainActivity.GREFFE_FILENAME,
                PERSONNE to MainActivity.PERSONNE_FILENAME,
                STATUT to MainActivity.STATUT_FILENAME)
    }

    @JavascriptInterface
    fun fetchFile(url: String): String
    {
        return context.openFileInput(URL_AND_FILE[url])
                .bufferedReader().use { it.readText() }
    }
}
