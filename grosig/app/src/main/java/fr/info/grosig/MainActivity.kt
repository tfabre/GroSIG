package fr.info.grosig

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.webkit.GeolocationPermissions
import android.webkit.WebChromeClient
import android.webkit.WebView
import fr.info.grosig.js.FileFetcher

class MainActivity : AppCompatActivity()
{
    companion object
    {
        private const val SERVER_URL = "http://192.168.1.42:8800"

        private const val SERVER_URL_FOR_LIEU_JUSTICE = "$SERVER_URL/geo/lieu_justice"
        const val LIEU_JUSTICE_FILENAME = "lieu_justice"

        private const val SERVER_URL_FOR_TGI = "$SERVER_URL/geo/tgi"
        const val TGI_FILENAME = "tgi"

        private const val SERVER_URL_FOR_TI = "$SERVER_URL/geo/ti"
        const val TI_FILENAME = "ti"

        private const val SERVER_URL_FOR_GREFFE = "$SERVER_URL/geo/greffe"
        const val GREFFE_FILENAME = "greffe"

        private const val SERVER_URL_FOR_PERSONNE = "$SERVER_URL/geo/personne"
        const val PERSONNE_FILENAME = "personne"

        private const val SERVER_URL_FOR_STATUT = "$SERVER_URL/statuts"
        const val STATUT_FILENAME = "status"

        private val URL_AND_FILE = hashMapOf(
                SERVER_URL_FOR_LIEU_JUSTICE to LIEU_JUSTICE_FILENAME,
                SERVER_URL_FOR_TGI to TGI_FILENAME,
                SERVER_URL_FOR_TI to TI_FILENAME,
                SERVER_URL_FOR_GREFFE to GREFFE_FILENAME,
                SERVER_URL_FOR_PERSONNE to PERSONNE_FILENAME,
                SERVER_URL_FOR_STATUT to STATUT_FILENAME)
    }

    private fun isNetworkAvailable(): Boolean
    {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val webView = findViewById<WebView>(R.id.web_view)

        if (Build.VERSION.SDK_INT >= 23)
            PermissionManager(this).askForPermission()

        webView.settings.javaScriptEnabled = true
        webView.settings.setGeolocationEnabled(true)
        webView.settings.setAppCacheEnabled(true)
        webView.settings.javaScriptCanOpenWindowsAutomatically = true
        webView.settings.databaseEnabled = true
        webView.settings.domStorageEnabled = true
        webView.addJavascriptInterface(fr.info.grosig.js.Location(this), "AndroidLocation")
        webView.webChromeClient = object: WebChromeClient()
        {
            override fun onGeolocationPermissionsShowPrompt(origin: String,
                                                            callback: GeolocationPermissions.Callback)
            {
                callback.invoke(origin, true, false)
            }
        }

        if (isNetworkAvailable())
        {
            // Download data files to internal storage
            URL_AND_FILE.forEach { entry ->
                Downloader(openFileOutput(entry.value, Context.MODE_PRIVATE)).execute(entry.key)
            }
            webView.loadUrl("$SERVER_URL/")
        }
        else
        {
            webView.addJavascriptInterface(FileFetcher(this), "FileFetcher")
            webView.loadUrl("file:///android_asset/index.html")
        }
    }
}
