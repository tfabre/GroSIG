//custom fonction for greffe style | edit here for desired syle
function greffeStyle(size)
{
    return createStyle(size, '#FFFF00', '#FF8C00');
}

//custom fonction for lieu_justice style | edit here for desired syle
function lieu_justiceStyle(size)
{
    return createStyle(size, '#FF0000', '#DF0101');
}

//custom fonction for tgi style | edit here for desired syle
function tgiStyle(size)
{
    return createStyle(size, '#3ADF00', '#31B404');
}

//custom fonction for ti style | edit here for desired syle
function tiStyle(size)
{
    return createStyle(size,'#00BFFF', '#01A9DB');
}

//custom fonction for ti style | edit here for desired syle
function personStyle(size)
{
    return createStyle(size, '#8000FF', '#5F04B4');
}

let createStyle = (size, circleColor, textColor) => new ol.style.Style
    ({
        image: new ol.style.Circle
        ({
            radius: zoom+size >= zoom+30 ? zoom+30 : zoom+size,
            stroke: new ol.style.Stroke({ color: circleColor, width : 4 }),
        }),

        text: new ol.style.Text
        ({
            text: size.toString(),
            fill: new ol.style.Fill({ color: textColor })
        })
    });
