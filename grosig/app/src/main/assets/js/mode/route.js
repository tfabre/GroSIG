let routeModule = new class
{
    constructor()
    {
        this.url_osrm_nearest = '//router.project-osrm.org/nearest/v1/driving/';
        this.url_osrm_route = '//router.project-osrm.org/route/v1/driving/';
        this.vectorRoutingSource = new ol.source.Vector();
        this.vectorRoutingLayer = new ol.layer.Vector({
            source: this.vectorRoutingSource
        });
        this.routeStyles = {
            route: new ol.style.Style({
                stroke: new ol.style.Stroke({
                    width: 6,
                    color: [254, 194, 0, 0.8]
                })
            }),
            icon: new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 5,
                    stroke: new ol.style.Stroke({color: '#FE7C00', width: 4}),
                })
            }),
        };
        let that = this;
        this.utils = {
            getNearest(coord)
            {
                const coord4326 = this.to4326(coord);
                return new Promise((resolve, reject) =>
                {
                    //make sure the coord is on street
                    fetch(that.url_osrm_nearest + coord4326.join()).then(response =>
                    {
                        // Convert to JSON
                        return response.json();
                    }).then(json =>
                    {
                        if (json.code === 'Ok') resolve(json.waypoints[0].location);
                        else reject();
                    });
                });
            },
            createFeature(coord)
            {
                let feature = new ol.Feature({
                    type: 'place',
                    geometry: new ol.geom.Point(ol.proj.fromLonLat(coord))
                });
                feature.setStyle(that.routeStyles.icon);
                that.vectorRoutingSource.addFeature(feature);
            },
            createRoute(polyline)
            {
                // route is ol.geom.LineString
                const route = new ol.format.Polyline({
                    factor: 1e5
                }).readGeometry(polyline, {
                    dataProjection: 'EPSG:4326',
                    featureProjection: 'EPSG:3857'
                });
                let feature = new ol.Feature({
                    type: 'route',
                    geometry: route
                });
                feature.setStyle(that.routeStyles.route);
                that.vectorRoutingSource.addFeature(feature);
            },
            to4326(coord)
            {
                return ol.proj.transform([parseFloat(coord[0]), parseFloat(coord[1])], 'EPSG:3857', 'EPSG:4326');
            }
        };
    }

    get layer()
    {
        return this.vectorRoutingLayer;
    }
    // map.addLayer(vectorRoutingLayer);

    registerRoutingMode(applicationMode)
    {
        applicationMode.setActionForMode('routing', (evt, ctx) =>
        {
            /** @param { Array } coord_street */
            this.utils.getNearest(evt.coordinate).then(coord_street =>
            {
                const user_location = ctx.userLocation;
                this.vectorRoutingSource.clear();
                this.utils.createFeature(ctx.userLocation);
                this.utils.createFeature(coord_street);

                //get the route
                const point1 = user_location.join();
                const point2 = coord_street.join();

                fetch(this.url_osrm_route + point1 + ';' + point2).then(r => r.json())
                    .then(json =>
                    {
                        if (json.code !== 'Ok')
                        {
                            return;
                        }

                        this.utils.createRoute(json.routes[0].geometry);
                        hideAlerts();
                        document.getElementById('routing-result-alert').style.display = 'inline';
                        document.getElementById('routing-distance-result').innerText =
                            formatLengthFromMeter(json.routes[0].distance);
                        document.getElementById('routing-duration-result').innerText =
                            this.formatDurationFromSecond(json.routes[0].duration);

                        applicationMode.mode = "global";
                    });
            });
        });
    }

    formatDurationFromSecond(seconds)
    {
        if (seconds < 60)
            return seconds + ' s';

        let minutes = ~~(seconds / 60);

        if (minutes < 60)
            return minutes + ' min';

        let hours = ~~(minutes / 60);
        minutes = minutes % 60;

        return hours + ' h' +
            (minutes > 0 ? ' ' + minutes + ' min' : '');
    }

    enableRoutingMode()
    {
        applicationMode.mode = "routing";
        hideAlerts();
        document.getElementById('routing-mode-alert').style.display = 'inline';
    }
};