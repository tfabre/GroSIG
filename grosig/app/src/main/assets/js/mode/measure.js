/**
 * Format length output.
 * @param {ol.geom.LineString} line The line.
 * @return {string} The formatted length.
 */
let formatLength = line =>
    formatLengthFromMeter(ol.Sphere.getLength(line));


function formatLengthFromMeter(length)
{
    return (length > 1000) ?
        `${(Math.round(length / 1000 * 100) / 100)} km` :
        `${(Math.round(length * 100) / 100)} m`;
}

function enableMeasureMode()
{
    applicationMode.mode = "measure";
    hideAlerts();
    document.getElementById('measure-mode-alert').style.display = 'inline';
}

function registerMeasureMode(applicationMode)
{
    applicationMode.setActionForMode("measure", (evt, ctx) =>
    {
        const coordiante = evt.coordinate;
        const currentPostion = ol.proj.transform(ctx.userLocation, 'EPSG:4326', 'EPSG:3857');

        hideAlerts();
        document.getElementById('measure-result-alert').style.display = 'inline';
        document.getElementById('measure-result').innerText =
            formatLength(new ol.geom.LineString([currentPostion, coordiante]));

        applicationMode.mode = "global";
    });
}