let statuts = {};
xhrGet("/statuts", data =>
        JSON.parse(data).forEach(elem =>
            statuts[elem.id] = elem.nom_statut),
    e =>
        console.log(`erreur xhr : ${e}`));

function registerGlobalMode(applicationMode)
{
    applicationMode.setActionForMode("global", (evt, ctx) =>
    {
        const coordinate = evt.coordinate;
        let elem = document.createElement("div");
        elem.className = "scroll";
        ctx.map.forEachFeatureAtPixel(evt.pixel, function (feature)
        {
            feature.get("features").forEach(function (fts)
            {
                if (elem.innerText.length > 0)
                {
                    elem.appendChild(document.createElement("hr"));
                }
                let featureProperties = fts.getProperties();
                let paragraph = document.createElement("p");
                if (fts.getId().split(".")[0] === "greffe")
                {
                    paragraph.innerHTML = featureProperties.adresse1 + "<br>" +
                        "<a href='" + featureProperties.site_web + "'>" + featureProperties.site_web + "</a>" + "<br>" +
                        featureProperties.telephone;
                }
                else if (fts.getId().split(".")[0] === 'personne')
                {
                    paragraph.innerHTML = `${featureProperties.nom_personne}<br>${featureProperties.adresse}<br>${statuts[featureProperties.statut]}`;
                }
                else
                {
                    paragraph.innerHTML = featureProperties.intitule;
                }
                elem.appendChild(paragraph);
            });

            // remove all element from content
            while (ctx.content.firstChild) ctx.content.removeChild(ctx.content.firstChild);
            ctx.content.appendChild(elem);
            ctx.overlay.setPosition(coordinate);
        });
    });
}