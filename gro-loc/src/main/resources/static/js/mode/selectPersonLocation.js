let updateMap = () => {};

function toggleSelectPerson()
{
    applicationMode.mode = "selectPersonLocation";
    hideAlerts();
    document.getElementById('select-person-div').style.display = 'inline';
    hideAddPersonAlert();
}

function savePerson()
{
    const name = document.getElementById("name").value;
    const address = document.getElementById("address").value;
    const statutDropdown = document.getElementById("statut");
    const statut = statutDropdown.options[statutDropdown.selectedIndex].value;
    const coords = document.getElementById("coords").value;
    const data =
        `{"nom_personne": "${name}", "adresse": "${address}","coord":{"type":"Point","coordinates":[${coords}]}, "statut":{"id":${statut}}}`;
    xhrPost("/personne", data, () =>
    {
        hideAlerts();
        document.getElementById('alert-div').style.display = 'inline';
        updateMap();
    });
}

function showAddPersonModal(coords)
{
    // selectPersonLocation = false;
    applicationMode.mode = "global";
    hideSelectPersonCoordsAlert();
    $('#addPersonModal').modal('show');
    if (document.getElementById("statut").childElementCount === 0)
    {
        xhrGet("/statuts", data =>
            {
                const jsonStatuts = JSON.parse(data);
                let dropdown = document.getElementById("statut");
                jsonStatuts.forEach(function(elem)
                {
                    let opt = document.createElement('option');
                    opt.value = elem.id;
                    opt.innerHTML = elem.nom_statut;
                    dropdown.appendChild(opt);
                    statuts[elem.id] = elem.nom_statut;
                })
            },
            e => console.log(`erreur hxr : ${e}`));
    }
    document.getElementById("coords").value = coords;
}

function hideAddPersonAlert()
{
    document.getElementById('alert-div').style.display = 'none';
}

function hideSelectPersonCoordsAlert()
{
    document.getElementById('select-person-div').style.display = 'none';
}

function registerSelectPersonLocationMode(applicationMode)
{
    applicationMode.setActionForMode("selectPersonLocation", evt =>
    {
        const coordinate = evt.coordinate;
        showAddPersonModal(ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326'));
    });
}