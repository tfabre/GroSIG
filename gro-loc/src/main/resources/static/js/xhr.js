// xhr GET to our server
function xhrGet(_url, onSuccessCallback, onErrorCallback)
{
    const isOffline = typeof FileFetcher !== 'undefined';
    if (isOffline)
    {
        onSuccessCallback(FileFetcher.fetchFile(_url))
    }
    else
    {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', _url);

        xhr.onload = function()
        {
            if (xhr.status === 200)
                onSuccessCallback(xhr.responseText);
            else
                onErrorCallback(xhr.status);
        };

        xhr.onerror = function(e) { onErrorCallback(e); };

        xhr.send();
    }

}

// xhr POST to our server
function xhrPost(_url, data, onSuccess)
{
    let xhr = new XMLHttpRequest();
    xhr.open('POST', _url, true);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onreadystatechange = function()
    {
        if (xhr.readyState === XMLHttpRequest.DONE && (xhr.status >= 200 && xhr.status < 300))
        {
            onSuccess();
        }
    };
    xhr.send(data);
}