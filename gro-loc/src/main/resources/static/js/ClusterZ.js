// class for cluster ~ make it easier
function ClusterZ(zoom)
{
    this.vector_src; //raw features
    this.cluster_src; //regroup features as cluster
    this.cluster_style_cache; //cache for cluster
    this.cluster_layer_vector; //cluster that will be displayed (with style, see below)
    this.style;
    this.groupingDistance = 100 - zoom; //how much we regroup, reduce distance as we zoom in

    //hold ref for inner use
    let clusterZ = this;

    this.load = function(_base_url)
    {
        const isOffline = typeof FileFetcher !== 'undefined';
        if (isOffline)
        {
            const geojsonObject = JSON.parse(FileFetcher.fetchFile(_base_url));
            const features = geojsonObject.features.filter(feature => feature.geometry !== null)
            geojsonObject.features = features;

            this.vector_src = new ol.source.Vector({
                features: (new ol.format.GeoJSON()).readFeatures(geojsonObject, {featureProjection : 'EPSG:3857'})
            });
            console.log("dd" + _base_url);
        }
        else
        {
            this.vector_src = new ol.source.Vector
            ({
                format: new ol.format.GeoJSON(),
                strategy: ol.loadingstrategy.bbox,
                loader: function(extent, resolution, projection)
                {
                    let proj = projection.getCode();
                    let url = _base_url + extent.join(',') + ',' + proj;

                    xhrGet(url,
                        data =>
                        {
                            let features = clusterZ.vector_src.getFormat().readFeatures(data, {featureProjection : proj});
                            clusterZ.vector_src.addFeatures(features);
                        },
                        e =>
                        {
                            console.log("erreur hxr : "+e);
                            clusterZ.vector_src.removeLoadedExtent(extent) ;
                        });
                },
            });
        }

        this.cluster_src = new ol.source.Cluster
        ({
            source: clusterZ.vector_src,
            distance: clusterZ.groupingDistance
        });

        this.cluster_style_cache = {};
        this.cluster_layer_vector = new ol.layer.Vector
        ({
            source: clusterZ.cluster_src,
            style: feature =>
            {
                let size = feature.get('features').length;
                let style = clusterZ.cluster_style_cache[size];

                if (!style)
                {
                    style = clusterZ.style(size);
                    clusterZ.cluster_style_cache[size] = style;
                }

                return style;
            }
        });
    };
}