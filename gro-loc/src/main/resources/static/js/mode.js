class Mode
{
    constructor()
    {
        this.modesMap = {"global": () => {}};
        this.currentMode = "global";
    }

    setActionForMode(mode, action)
    {
        this.modesMap[mode] = action;
    }

    get modes()
    {
        return Object.keys(this.modesMap);
    }

    set mode(mode)
    {
        return this.currentMode = mode;
    }

    get action()
    {
        return this.modesMap[this.currentMode];
    }
}