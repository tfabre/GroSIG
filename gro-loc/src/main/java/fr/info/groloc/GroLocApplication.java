package fr.info.groloc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroLocApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(GroLocApplication.class, args);
    }
}
