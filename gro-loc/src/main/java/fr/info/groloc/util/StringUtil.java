package fr.info.groloc.util;

import java.util.Arrays;

/**
 * Utility class for strings.
 */
public final class StringUtil
{
    /**
     * Check if a string is null or empty.
     *
     * @param str String to check.
     * @return True if string is null or empty, false otherwise.
     */
    public static boolean isNullOrEmpty(String str)
    {
        return str == null || str.isEmpty();
    }

    /**
     * Check if any string in list is null or empty.
     *
     * @param strings Strings to check.
     * @return True if one of the strings is null or empty. False if there are all not null and not empty.
     */
    public static boolean areNullOrEmpty(String... strings)
    {
        return Arrays.stream(strings).anyMatch(StringUtil::isNullOrEmpty);
    }
}