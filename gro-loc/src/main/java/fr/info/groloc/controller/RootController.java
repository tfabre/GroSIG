package fr.info.groloc.controller;

import fr.info.groloc.dao.PersonneRepository;
import fr.info.groloc.dao.StatutRepository;
import fr.info.groloc.entity.Personne;
import fr.info.groloc.entity.Statut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class RootController
{
    @Autowired
    private StatutRepository statutRepository;
    @Autowired
    private PersonneRepository personneRepository;

    @RequestMapping(value = "/personne", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Collection<Personne> getPersonnes()
    {
        return (Collection<Personne>) personneRepository.findAll();
    }

    @RequestMapping(value = "/personne", method = RequestMethod.POST)
    public ResponseEntity<String> savePerson(@RequestBody Personne personne)
    {
        personneRepository.save(personne);
        return ResponseEntity.accepted().build();
    }

    @RequestMapping(value = "/statuts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Collection<Statut> getStatuts()
    {
        return (Collection<Statut>) statutRepository.findAll();
    }
}
