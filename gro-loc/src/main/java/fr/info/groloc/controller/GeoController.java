package fr.info.groloc.controller;

import fr.info.groloc.util.StringUtil;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Logger;

@RestController
@RequestMapping(value = "/geo")
public final class GeoController
{
    private static final Logger LOGGER = Logger.getLogger(GeoController.class.getName());

    private static String getJsonFromGeoServer(final String type, final String bbox)
    {
        final StringBuilder urlBuilder = new StringBuilder("http://localhost:8080/geoserver/groloc/" +
                "ows?service=WFS&version=1.0.0&request=GetFeature&outputFormat=application%2Fjson" +
                "&typeName=groloc:");
        urlBuilder.append(type);

        if (!StringUtil.isNullOrEmpty(bbox))
        {
            urlBuilder.append("&bbox=");
            urlBuilder.append(bbox);
        }

        final String geoserverUrl = urlBuilder.toString();
        LOGGER.info(geoserverUrl);

        try
        {
            final URL url = new URL(urlBuilder.toString());
            final URLConnection connection = url.openConnection();
            connection.connect();

            final StringBuilder buffer = new StringBuilder();
            try (final BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream())))
            {
                String line;
                while ((line = br.readLine()) != null)
                    buffer.append(line);
            }
            return buffer.toString();
        }
        catch (Exception e)
        {
            LOGGER.info(e.getMessage());
        }
        return "{}";
    }

    @RequestMapping(value = "/greffe", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getGreffes()
    {
        return getJsonFromGeoServer("greffe", null);
    }

    @RequestMapping(value = "/greffes/bbox/{bbox:.+}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getGreffesBbox(@PathVariable("bbox") String bbox)
    {
        return getJsonFromGeoServer("greffe", bbox);
    }

    @RequestMapping(value = "/lieu_justice", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getLieujustice()
    {
        return getJsonFromGeoServer("lieu_justice", null);
    }

    @RequestMapping(value = "/lieu_justice/bbox/{bbox:.+}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getLieujusticeBbox(@PathVariable("bbox") String bbox)
    {
        return getJsonFromGeoServer("lieu_justice", bbox);
    }

    @RequestMapping(value = "/ti", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getTi()
    {
        return getJsonFromGeoServer("ti", null);
    }

    @RequestMapping(value = "/ti/bbox/{bbox:.+}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getTi(@PathVariable("bbox") String bbox)
    {
        return getJsonFromGeoServer("ti", bbox);
    }

    @RequestMapping(value = "/tgi", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getTgi()
    {
        return getJsonFromGeoServer("tgi", null);
    }

    @RequestMapping(value = "/tgi/bbox/{bbox:.+}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getTgi(@PathVariable("bbox") String bbox)
    {
        return getJsonFromGeoServer("tgi", bbox);
    }

    @RequestMapping(value = "/personne", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getPersonnes()
    {
        return getJsonFromGeoServer("personne", null);
    }

    @RequestMapping(value = "/personne/bbox/{bbox:.+}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getPersonnes(@PathVariable("bbox") String bbox)
    {
        return getJsonFromGeoServer("personne", bbox);
    }
}
