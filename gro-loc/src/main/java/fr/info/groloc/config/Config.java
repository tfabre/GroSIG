package fr.info.groloc.config;

import com.bedatadriven.jackson.datatype.jts.JtsModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config
{
    @Bean
    JtsModule addJsonGeometrySupport()
    {
        return new JtsModule();
    }
}
