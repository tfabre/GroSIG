package fr.info.groloc.dao;

import fr.info.groloc.entity.Ti;
import org.springframework.data.repository.CrudRepository;

public interface TiRepository extends CrudRepository<Ti, String>
{
}
