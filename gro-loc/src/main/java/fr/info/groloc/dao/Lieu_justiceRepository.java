package fr.info.groloc.dao;

import fr.info.groloc.entity.Lieu_justice;
import org.springframework.data.repository.CrudRepository;

public interface Lieu_justiceRepository extends CrudRepository<Lieu_justice, String>
{
}
