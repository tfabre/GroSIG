package fr.info.groloc.dao;

import fr.info.groloc.entity.Statut;
import org.springframework.data.repository.CrudRepository;

public interface StatutRepository extends CrudRepository<Statut, Integer>
{
}
