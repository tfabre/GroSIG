package fr.info.groloc.dao;

import fr.info.groloc.entity.Personne;
import org.springframework.data.repository.CrudRepository;

public interface PersonneRepository extends CrudRepository<Personne, Long>
{

}
