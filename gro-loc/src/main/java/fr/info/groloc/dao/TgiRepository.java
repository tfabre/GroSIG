package fr.info.groloc.dao;

import fr.info.groloc.entity.Tgi;
import org.springframework.data.repository.CrudRepository;

public interface TgiRepository extends CrudRepository<Tgi, String>
{
}
