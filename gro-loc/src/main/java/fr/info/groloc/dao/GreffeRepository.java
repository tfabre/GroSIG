package fr.info.groloc.dao;

import fr.info.groloc.entity.Greffe;
import org.springframework.data.repository.CrudRepository;

public interface GreffeRepository extends CrudRepository<Greffe, String>
{
}
