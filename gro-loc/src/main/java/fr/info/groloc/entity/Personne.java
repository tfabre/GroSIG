package fr.info.groloc.entity;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;

import javax.persistence.*;

@Entity
public class Personne
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;
    private String nom_personne;
    private String adresse;

    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(contentUsing = GeometryDeserializer.class)
    private Geometry coord;

    @ManyToOne
    @JoinColumn(name = "statut")
    private Statut statut;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getNom_personne() { return nom_personne; }

    public void setNom_personne(String nom_personne) { this.nom_personne = nom_personne; }

    public String getAdresse() { return adresse; }

    public void setAdresse(String adresse) { this.adresse = adresse; }

    public Geometry getCoord() { return coord; }

    public void setCoord(Geometry coord) { this.coord = coord; }

    public Statut getStatut() { return statut; }

    public void setStatut(Statut statut) { this.statut = statut; }

    @Override
    public String toString()
    {
        return nom_personne;
    }
}
