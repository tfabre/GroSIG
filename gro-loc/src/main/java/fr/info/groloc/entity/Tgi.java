package fr.info.groloc.entity;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Tgi {
    @Id
    private String id;
    private String intitule;
    private String categorie;
    private String dila;
    private String site_web;
    private String adresse1;
    private String adresse2;
    private String code_insee;
    private String code_postal;
    private String commune;
    private String telephone;
    private String fax;
    private String horaire_ouverture;

    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(using = GeometryDeserializer.class)
    private Geometry coord;
    private int precision_coord;


    public String getId()
    {
        return id;
    }

    public String getIntitule()
    {
        return intitule;
    }

    public String getCategorie()
    {
        return categorie;
    }

    public String getDila()
    {
        return dila;
    }

    public String getSite_web()
    {
        return site_web;
    }

    public String getAdresse1()
    {
        return adresse1;
    }

    public String getAdresse2()
    {
        return adresse2;
    }

    public String getCode_insee()
    {
        return code_insee;
    }

    public String getCode_postal()
    {
        return code_postal;
    }

    public String getCommune()
    {
        return commune;
    }

    public String getTelephone()
    {
        return telephone;
    }

    public String getFax()
    {
        return fax;
    }

    public String getHoraire_ouverture()
    {
        return horaire_ouverture;
    }

    public Geometry getCoord()
    {
        return coord;
    }

    public int getPrecision_coord()
    {
        return precision_coord;
    }
}