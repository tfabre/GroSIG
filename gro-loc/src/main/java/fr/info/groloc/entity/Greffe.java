package fr.info.groloc.entity;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Greffe
{
    @Id
    private String id;
    private String nom_greffe;
    private String adresse1;
    private String adresse2;
    private String code_postal;
    private String ville;
    private String telephone;
    private String fax;
    private String site_web;

    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(contentUsing = GeometryDeserializer.class)
    private Geometry coord;
    private String horaire_ouverture;
    private String greffe_normalise;
    private String site_donnee;

    public String getId()
    {
        return id;
    }

    public String getNom_greffe()
    {
        return nom_greffe;
    }

    public String getAdresse1()
    {
        return adresse1;
    }

    public String getAdresse2()
    {
        return adresse2;
    }

    public String getCode_postal()
    {
        return code_postal;
    }

    public String getVille()
    {
        return ville;
    }

    public String getTelephone()
    {
        return telephone;
    }

    public String getFax()
    {
        return fax;
    }

    public String getSite_web()
    {
        return site_web;
    }

    public Geometry getCoord()
    {
        return coord;
    }

    public String getHoraire_ouverture()
    {
        return horaire_ouverture;
    }

    public String getGreffe_normalise()
    {
        return greffe_normalise;
    }

    public String getSite_donnee()
    {
        return site_donnee;
    }

    @Override
    public String toString()
    {
        return String.format("GREFFE::{%s, %s}", nom_greffe, coord);
    }
}
