package fr.info.groloc.entity;

import javax.persistence.*;

@Entity
public class Statut
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;
    private String nom_statut;

    public Integer getId() { return id; }

    public void setId(Integer id) { this.id = id; }

    public String getNom_statut() { return nom_statut; }

    public void setNom_statut(String nom_statut) { this.nom_statut = nom_statut; }

    @Override
    public String toString()
    {
        return nom_statut;
    }
}
